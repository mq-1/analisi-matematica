# 25/09/2019

* Se $A$ non è superiormente limitato si pone $supA = + \infty$
* Se $A$ non è inferiormente limitato si pone $supA = - \infty$

### Es

$A = \{ x \in R | x < 7 \} = (-\infty, 7)$\
$supA = 7$\
inf = $- \infty$

$exp_a: R \underset{su}{\overset{1-1}{\to}} R^*_+$

## Def

Si $a \in R^*_+ \setminus \{1\}$, chiamiamo **funzione logaritmo** in base $a$ l'inversa di $exp_a$:

$\log_a: R^*_+ \to R$\
$log_a = (exp_a)^{-1}$

* $log_2 8 = 3$

$log_a b$ = esponente da dare ad $a$ per ottenere $b$ cioè:\
$a^{(log_b)} = b$

## Proprietà

1. $\forall x, y \in R^*_+, log_a (xy) = log_a x + log_a y$
1. $\forall x, y \in R^*_+, log_a (\frac x y) = log_a x - log_a y$
1. $\forall x, y \in R^*_+, \alpha \in R, \quad log_a (x^\alpha) = \alpha \log_a x y$
1. $\log_b y = \frac {\log_a y} {\log_a b} \quad a,b \in R^*_+ \setminus \{1\}, y \in R^*_+$\
   Es. $\log_2 y = \frac {log_3 y} {\log_3 2}$

---

$e$ = *numero di Nepero*

$2 < e < 3$

Notazione:\
$\log x = \log_e x$\
$\ln x = \log_e x$\
$Log_x = \log_{10} x$

## Teor

Sia $a \in R^*_+ \setminus \{1\}$

1. Se $a > 1 \implies \log_a$ è strettamente crescente
1. Se $a < 1 \implies \log_a$ è strettamente decrescente
1. Se $a > 1 \implies \log_a$ è strettamente crescente
1. $\log_a: R^*_+ \underset{su}{\overset{1-1}{\to}} R$

---

$a > 1$\
$y = \log_a x$

```{r echo=FALSE}
f = function(x){log(x, exp(1))}
curve(f)
```

$a > 1$\
$y = \log_a x$

```{r echo=FALSE}
f = function(x){log(x, exp(-1))}
curve(f)
```

## Funzioni circolari

Sia $U = \{ (x,y) \in R^2 | x^2 + y^2 = 1 \}$

$\cos: R \to R$\
$\alpha \mapsto x_p$

$\sin: R \to R$\
$\alpha \mapsto y_p$

### Proprietà

cos e sin sono *periodiche*, di periodo $2\pi$, cioè:\
$\cos(\alpha + 2k\pi) = \cos \alpha \quad k \in N$\
$\sin(\alpha + 2k\pi) = \sin \alpha$

$\sin (-\alpha) = -\sin \alpha$\
$\cos (-\alpha) = \cos \alpha$

$\sin (\pi - \alpha) = \sin \alpha$\
$\cos (\pi - \alpha) = -\cos \alpha$

* $\cos^2 \alpha + \sin^2 \alpha = 1 \quad \forall \alpha \in R$
* $\underbrace{|\cos \alpha| \leq 1}_{-1 \leq \cos \alpha \leq 1}, \underbrace{|\sin \alpha| \leq 1}_{-1 \leq \sin \alpha \leq 1}$

### Grafici

cos

```{r echo=FALSE}
f = function(x){cos(x)}
curve(f, from=-10, to=10)
```

sin

```{r echo=FALSE}
f = function(x){sin(x)}
curve(f, from=-10, to=10)
```

### Osservazione

sin e cos non sono monotone (definite in R) ma lo sono se ristrette a certi intervalli

* cos è
  - strettamente crescente su $(\pi, 2\pi)$
  - strettamente decrescente su $(0, \pi)$
* sin è
  - strettamente crescente su $(- \frac \pi 2, \frac \pi 2)$
  - strettamente decrescente su $(\frac \pi 2, \frac 3 2 \pi)$

### Formule

1. $\forall x_1, x_2 \in R, \quad \cos(x_1, x_2) = \cos x_1 * \cos x_2 - \sin x_1 * \sin x_2$
1. $\forall x_1, x_2 \in R, \quad \sin(x_1, x_2) = \sin x_1 * \cos x_2 + \cos x_1 * \sin x_2$
1. $\cos (2x) = \cos^2 x - \sin^2 x$ (duplicazione)
1. $\sin (2x) = 2 \sin x \cos x$ (duplicazione)
1. $|\cos (\frac x 2)| = \sqrt {\frac {1 + \cos x} 2}$ (bisezione)
1. $|\sin (\frac x 2)| = \sqrt {\frac {1 - \cos x} 2}$ (bisezione)

### Tangente-cotangente

$\tan \alpha = y_p$\
$p = (1, \tan \alpha)$

$\tan \alpha = \frac {\sin \alpha} {\cos \alpha}$

```{r echo=FALSE}
f = function(x){tan(x)}
curve(f, from=-10, to=10)
```
